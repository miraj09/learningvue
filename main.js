const eventBus = new Vue()

Vue.component('product-details', {
  props: {
    details: {
      type: Array,
      required: true,
    },
    sizes: {
      type: Array,
      required: true,
    },
  },
  template: `
    <div>
      <h3>Details</h3>
      <ul>
        <li v-for="detail in details">{{ detail }}</li>
      </ul>
      <h3>Size</h3>
      <ul>
        <li v-for="size in sizes">{{ size }}</li>
      </ul>
    </div>
  `,
})

Vue.component('product', {
  props: {
    premium: {
      type: Boolean,
      required: true,
    },
  },
  template: `
    <div class="product">

      <div class="product-image">
        <img :src="image" :alt="description">
      </div>

      <div class="product-info">
        <h1>{{ product }}</h1>
        <p v-if="inStock">In Stock</p>
        <p v-else>Out Of Stock</p>
        <span v-if="onSale" :class="{ outOfStock: !inStock }">On Sale!</span>

        <p>Shipping: {{ shipping }}</p>
          <product-details :details="details" :sizes="sizes"></product-details>

        <div class="color-box"
        v-for="(variant, index) in variants"
        :style="{ backgroundColor: variant.variantColor }"
        :key="variant.variantId"
        @mouseover="updateProduct(index)"
        >

        </div>

        <button v-on:click="addToCart" :disabled="!inStock" :class="{ disabledButton: !inStock }">Add To Cart</button>
        <button v-on:click="removeCart">Remove Cart</button>

      </div>

      <product-tabs :reviews="reviews"></product-tabs>

    </div>
  `,
  data() {
    return {
      product: 'Socks',
      description: 'A Pair Of Fuzzy Socks',
      selectedVariant: 0,
      onSale: true,
      details: ['80% cotton', '20% polyester', 'Gender-neutral'],
      variants: [
        {
          variantId: 2234,
          variantColor: 'green',
          variantImage: './images/vmSocks-green.jpg',
          variantQuantity: 5,
        },
        {
          variantId: 2235,
          variantColor: 'blue',
          variantImage: './images/vmSocks-blue.jpg',
          variantQuantity: 0,
        },
      ],
      sizes: ['S', 'M', 'L', 'XL', 'XXL', 'XXXL'],
      reviews: [],
    }
  },
  methods: {
    addToCart() {
      this.$emit('add-to-cart', this.variants[this.selectedVariant].variantId)
    },
    removeCart() {
      this.$emit('remove-from-cart', this.variants[this.selectedVariant].variantId)
    },
    updateProduct(index) {
      this.selectedVariant = index
    },
  },
  computed: {
    image() {
      return this.variants[this.selectedVariant].variantImage
    },
    inStock() {
      return this.variants[this.selectedVariant].variantQuantity
    },
    shipping() {
      if (this.premium) {
        return 'Free'
      }
      else {
        return '$2.99'
      }
    },
  },
  mounted() {
    eventBus.$on('review-submitted', (productReview) => {
      this.reviews.push(productReview)
    })
  },
})

Vue.component('product-review', {
  template: `
    <form action="" class="review-form" @submit.prevent="onSubmit">
  <p class="error" v-if="errors.length">
    <b>Please Correct The Following Error(s):</b>
    <ul>
      <li v-for="error in errors">{{ error }}</li>
    </ul>
  </p>

  <p>
    <label for="name">Name:</label>
    <input id="name" v-model="name" placeholder="name" />
  </p>

  <p>
    <label for="review">Review:</label>
    <textarea id="review" v-model="review"></textarea>
  </p>

  <p>
    <label for="rating">Rating:</label>
    <select name="" id="rating" v-model.number="rating">
      <option value="5">5</option>
      <option value="4">4</option>
      <option value="3">3</option>
      <option value="2">2</option>
      <option value="1">1</option>
    </select>
  </p>
  <p>
    <input type="submit" value="Submit" />
  </p>
</form>
  `,
  data() {
    return {
      name: null,
      review: null,
      rating: null,
      errors: [],

    }
  },
  methods: {
    onSubmit() {
      if (this.name && this.review && this.rating) {
        const productReview = {
          name: this.name,
          review: this.review,
          rating: this.rating,
        }
        eventBus.$emit('review-submitted', productReview)
        this.name = null
        this.review = null
        this.rating = null
        this.errors = []
      }
      else {
        if (!this.name) this.errors.push('Name Required.')
        if (!this.review) this.errors.push('Review Required.')
        if (!this.rating) this.errors.push('Rating Required.')
      }
    },
  },
})

Vue.component('product-tabs', {
  props: {
    reviews: {
      type: Array,
      required: false,
    },
  },

  template: `
    <div>
      <ul>
        <span class="tab"
              v-for="(tab, index) in tabs"
              @click="selectedTab = tab"
              :class="{ activeTab: selectedTab === tab }"
              :key="index"
        >{{ tab }}</span>
      </ul>

      <div v-show="selectedTab === 'Reviews'">
        <p v-if="!reviews.length">There Are No Reviews</p>
        <ul v-else>
          <li v-for="(review, index) in reviews" :key="index">
            <p>{{ review.name }}</p>
            <p>Rating: {{ review.rating }}</p>
            <p>{{ review.review }}</p>
          </li>
        </ul>
      </div>
      <div v-show="selectedTab === 'Make A Review'">
        <product-review></product-review>
      </div>
    </div>
  `,
  data() {
    return {
      tabs: ['Reviews', 'Make A Review'],
      selectedTab: 'Reviews',
    }
  },
})


const app = new Vue({
  el: '#app',
  data: {
    premium: false,
    cart: [],
  },
  methods: {
    addCart(id) {
      this.cart.push(id)
    },
    removeCart(id) {
      this.cart.pop(id)
    },
  },
})
